#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
import redis.clients.jedis.*

JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost")

return pool
