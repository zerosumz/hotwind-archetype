HotWind ArcheType
=================

[HotWind](https://github.com/ageldama/hotwind) hello archetype

## Usage

    mvn archetype:generate                                  
        -DarchetypeGroupId=kr.co.inger
        -DarchetypeArtifactId=hotwind-archetype      
        -DarchetypeVersion=0.0.1-SNAPSHOT                
        -DgroupId=<my.groupid>                                
        -DartifactId=<my-artifactId>